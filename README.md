# WebDriverAgent

Support 
- macOs 10.15+
- iOS Version 13.5.1 +


## Getting Started On This Repository

Download Git : (https://github.com/appium/WebDriverAgent)

Setting to WebDriverAgent.xcodeproj 
https://medium.com/@JatuphonChaisak/how-to-install-and-configuration-automation-on-mac-os-5a565d95d198

## Run appium with node
Copy WebDriverAgent to /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent

## Run appium desktop
Copy WebDriverAgent to /Applications/Appium.app/Contents/Resources/app/node_modules/appium/node_modules/appium-webdriveragent/

To get the project set up just run bootstrap script Appium Desktop:
```
cd /Applications/Appium.app/Contents/Resources/app/node_modules/appium/node_modules/appium-webdriveragent
./Scripts/bootstrap.sh -d
xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination 'id=<udid>' clean build test
```

To get the project set up just run bootstrap script Node Appium :
```
cd /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent
./Scripts/bootstrap.sh -d
xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination 'id=<udid>' clean build test
```
